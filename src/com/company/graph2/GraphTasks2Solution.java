package com.company.graph2;

import java.util.HashMap;
import com.company.graph2.oopgraphs.*;

public class GraphTasks2Solution implements GraphTasks2 {

    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        Digraph digraph = new Digraph(adjacencyMatrix);
        DijkstraAlgorithm algorithm = new DijkstraAlgorithm(digraph, startIndex);
        return algorithm.resultAsHashMap();
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        Graph graph = new Graph(adjacencyMatrix);
        PrimAlgorithm algorithm = new PrimAlgorithm(graph);
        return algorithm.getMinWeight();
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        Graph graph = new Graph(adjacencyMatrix);
        KruskalAlgorithm algorithm = new KruskalAlgorithm(graph);
        return algorithm.getMinWeight();
    }

}
